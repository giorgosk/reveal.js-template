# Slide 1

Slide with <span class="highlight">highlight</span>

---

# Slide 2

> Here is a nice styled blockquote

---

# Slide 3

```ruby
def addition
  a+b
end
```

---

# Slide 4 with fragment

```php
echo 'hello world';
```

- <!-- .element: class="fragment" --> Something with `code`
- <!-- .element: class="fragment" --> Something else with `code`